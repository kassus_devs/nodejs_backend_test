'use strict'

let countedMethod = require('./src/count');
let filteredMethod = require('./src/filter');

let command = ''

let app = () => {
    switch (command) {
    case 'node app.js --count':

        return countedMethod.countPeopleAndAnimals();
        
        break;
     case 'node app.js --filter=ry':

        return filteredMethod.filterPeopleAndAnimals();
        
        break;

    default:
        break;
}

}

