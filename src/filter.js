

'use strict'

// get the data from data.js we will use to make the filter
let data = require('../data');

// convert the data array to an custom object 
let dataObject = Object.assign({}, data);
let dataObjectToUse = dataObject;

filterPeopleAndAnimals();

/**
 * Loop into different object data from the global data from data.js 
 * return at the end the all data object width the array and object which has been filtered
 */


function filterPeopleAndAnimals() {
        // create a filter search value
        let ourSubstring = "ry";

        Object.keys(dataObjectToUse).forEach(function (key) {
            let allPeopleObject = dataObjectToUse[key];
            Object.keys(allPeopleObject).forEach(function (key) {
                let OnePeopleItem = allPeopleObject[key];
                Object.keys(OnePeopleItem).forEach(function (key) {
                    let OneAnimalObjectInPeopleItem = OnePeopleItem[key];
                    Object.keys(OneAnimalObjectInPeopleItem).forEach(function (key) {
                        let animalsOjectItem = OneAnimalObjectInPeopleItem[key];
                        Object.keys(animalsOjectItem).forEach(function (key) {
                            let oneAnimalsArray = animalsOjectItem[key];
                            Object.keys(oneAnimalsArray).forEach(function (key) {
                                let oneAnimalName = oneAnimalsArray[key];
                                // Parse oneAnimalName.name into String value and check if the string includes the search value 'ry
                                if (String(oneAnimalName.name).includes(ourSubstring)) {
                                    console.log('search succeed , ourSubstring belong to  ' + oneAnimalName.name);
                                } else {
                                    delete oneAnimalsArray[key].name;
                                    console.log('search failed , ourSubstring do not belong to  ' + oneAnimalName.name);
                                    // Create an empty object variable into to delete all empty itemobject in the animmals object
                                    let emptyObject = {};
                                    if (JSON.stringify(oneAnimalName) === JSON.stringify(emptyObject)) {
                                        delete oneAnimalsArray[key];
                                    }
                                };
                            })
                        })
                    })
                })
            })
        })

        // Display all data object width the custom item which has been filtered 
        console.log('Final filtered data ', JSON.stringify(dataObjectToUse));
    };




