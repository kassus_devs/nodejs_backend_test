
'use strict'


// get the data from data.js we will use to make the filter
let data = require('../data');

// convert the data array to an custom object 
let dataObject = Object.assign({}, data);
let dataObjectToUse = dataObject;

countPeopleAndAnimals();

/**
 * A loop is made into the object and get the counted numer of different array
 * return a counted array items
 */
function countPeopleAndAnimals  () {
    Object.keys(dataObjectToUse).forEach(function (key) {
        let firstpeopleObject = dataObjectToUse[key];
        Object.keys(firstpeopleObject).forEach(function (key) {
            let peopleObject = firstpeopleObject[key];
            // Create a custom people name item width the count of people include 
            peopleObject.name = peopleObject.name + ' ' + '[' + peopleObject.people.length + ']';
            Object.keys(peopleObject.people).forEach(function (key) {
                // Create a custom animal name item width the count of animal include 
                peopleObject.people[key].name = peopleObject.people[key].name + ' ' + '[' + peopleObject.people[key].animals.length + ']';
                // Display people object width the cstom item width the count of people
                console.log('People count ', peopleObject);
                // Display people and animals  object width the custom item width the count of people and count of animals
                console.log('People and animals count ', peopleObject.people[key]);
            })
            
        })
        // Display all data object  width the counts of People and Animals by counting the number of children and appending it in the name
        console.log('Final counted data', JSON.stringify(dataObjectToUse));
    })
}

