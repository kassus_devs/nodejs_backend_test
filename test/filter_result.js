
let filter_result =[{
    "name": "Dillauti [5]",
    "people": [{
        "name": "Winifred Graham [6]",
        "animals": [null, null, null, null, null, null]
    },
        {
            "name": "Blanche Viciani [8]",
            "animals": [null, null, null, null, null, null, null, null]
        },
        {
            "name": "Philip Murray [7]",
            "animals": [null, null, null, null, null, null, null]
        },
        {
            "name": "Bobby Ristori [9]",
            "animals": [null, null, null, null, null, null, null, null, null]
        },
        {
            "name": "Louise Pinzauti [5]",
            "animals": [null, null, null, null, null]
        }]
},
    {
        "name": "Tohabdal [8]",
        "people": [{
            "name": "Effie Houghton [7]",
            "animals": [null, null, null, null, null, null, null]
        },
            {
                "name": "Essie Bennett [7]",
                "animals": [null, null, null, null, null, null, null]
            },
            {
                "name": "Owen Bongini [5]",
                "animals": [null, null, null, null, null]
            }, {
                "name": "Alexander Fleury [7]",
                "animals": [null, null, null, null, null, null, null]
            },
            {
                "name": "Curtis Fuchs [6]",
                "animals": [null, null, null, null, null, null]
            },
            {
                "name": "Maud Lorenzo [7]",
                "animals": [null, null, null, null, null, null, null]
            },
            {
                "name": "Linnie Lamb [7]",
                "animals": [null, null, null, null, null, null, null]
            },
            {
                "name": "Randall Benoît [5]",
                "animals": [null, null, null, null, null]
            }]
    }, {
        "name": "Uzuzozne [7]",
        "people": [{
        "name":"Harold Patton [8]",
            "animals": [null, null, null, null, null, null, null, null]
        },
            { "name": "Millie Lapini [8]", "animals": [null, null, null, null, null, null, null, null] }, { "name": "Lillian Calamandrei [8]", "animals": [null, null, null, null, null, null, null, null] }, { "name": "Lina Allen [7]", "animals": [null, null, null, null, null, null, null] }, { "name": "Georgia Hooper [8]", "animals": [null, null, null, null, null, null, null, null] }, { "name": "Lillie Abbott [6]", "animals": [{ "name": "John Dory" }, null, null, null, null, null] }, { "name": "Philip Davis [8]", "animals": [null, null, null, null, null, null, null, null] }]
    },
    {
        "name": "Zuhackog [7]",
        "people": [{
            "name": "Elva Baroni [6]",
            "animals": [null, null, null, null, null, null]
        }, {
            "name": "Johnny Graziani [7]",
                "animals": [null, null, null, null, null, null, null]
            }, {
                "name": "Herman Christensen [7]",
                "animals": [null, null, null, null, null, null, null]
            },
            {
                "name": "Fannie Ancillotti [8]",
                "animals": [null, null, null, null, null, null, null, null]
            }, {
                "name": "Lawrence Camiciottoli [9]",
                "animals": [null, null, null, null, null, null, null, null, null]
            },
            {
                "name": "Marion Landi [6]",
                "animals": [null, null, null, null, null, null]
            }, {
                "name": "Lou de Bruin [5]",
                "animals": [null, null, null, null, null]
            }]
    }, {
        "name": "Satanwi [5]",
        "people": [{
            "name": "Elmer Kinoshita [7]",
            "animals": [null, null, null, null, null, null, null]
        }, {
            "name": "Cora Howell [7]",
                "animals": [null, null, null, null, null, null, null]
            }, {
                "name": "Ernest Conte [5]",
                "animals": [null, null, null, null, null]
            },
            {
                "name": "Dennis Franci [9]",
                "animals": [null, null, null, null, null, null, null, null, null]
            }, {
                "name": "Anthony Bruno [6]",
                "animals": [null, null, null, null, null,
                     { "name": "Oryx" }]
            }]
    }]