
'use strict'

let dataFortesting = require('./filter_result');
let filteredMethod  = require('../src/filter');

// get the data from data.js we will use to make the filter
let data = require('../data');


// Simple match test 
let assert = require('assert');
describe('Simple Math Test', () => {
 it('should return 2', () => {
        assert.equal(1 + 1, 2);
    });
 it('should return 9', () => {
        assert.equal(3 * 3, 9);
    });
});


// filter test 
describe("Filter Test ", function () {
    describe("Output Test", function () {
        it("Only animals containing `ry` are displayed ", function () {
               let actualData = filteredMethod;
            assert.strict.deepEqual(filteredMethod, dataFortesting);
        });
    });
    
    // test widthout filter method

    describe("Output Test", function () {
        it("No filter applied, it should return all data ", function () {
               let initialData = data;
            assert.strict.deepEqual(initialData, data);

        })
    })
});

// Length test 
describe("Length Test ",   function () {
    it("The count result should has the same length of 'data.js' ", function () {

    let initialDatajsLength =  dataFortesting.length ;
    let expected_length =  data.length ;
    assert.strict.equal( initialDatajsLength, expected_length) ;

    });
});




