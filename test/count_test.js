
let dataFortesting = require('./count_result');
let countedMethod = require('../src/count');

// get the data from data.js we will use to make the filter
let data = require('../data');


// Simple match test 
const assert = require('assert');
describe('Simple Math Test', () => {
 it('should return 2', () => {
        assert.equal(1 + 1, 2);
    });
 it('should return 9', () => {
        assert.equal(3 * 3, 9);
    });
});

// Count tests 
describe("Count Test ", function () {
    describe("Output Test", function () {
        it("It should print the counts of People and Animals by counting the number of children and appending it in the name ", function () {
            let actualData = countedMethod;
            assert.strict.deepEqual(actualData, dataFortesting);

        });
    })
});

// Length test 
describe("Length Test ",   function () {
    it("The count result should has the same length of 'data.js' ", function () {

    let initialDatajsLength =  dataFortesting.length ;
    let expected_length =  data.length ;
    assert.strict.equal( initialDatajsLength, expected_length) ;

    });
});
