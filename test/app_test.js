

'use strict'

var chai = require('chai');
var expect = chai.expect;
const should = require('chai').should();

let countedMethod = require('../src/count');
let filteredMethod = require('../src/filter');
let dataFortesting = require('./filter_result');

let command = ''

// get the data from data.js we will use to make the filter
let data = require('../data');


// Simple match test 
let assert = require('assert');
describe('Simple Math Test', () => {
 it('should return 2', () => {
        assert.equal(1 + 1, 2);
    });
 it('should return 9', () => {
        assert.equal(3 * 3, 9);
    });
});

// call counted method from app.js test

describe('should call countedMethod.countPeopleAndAnimals() immediately', () => {

    it("It should print the counts of People and Animals by counting the number of children and appending it in the name ", function () {
            let actualData = countedMethod;
            assert.strict.deepEqual(actualData, dataFortesting);

        });

});

// call filtered method from app.js test
  
describe('should call filteredMethod.filterPeopleAndAnimals() immediately', () => {
    it("nly animals containing `ry` are displayed  ", function () {
            let actualData = filteredMethod;
            assert.strict.deepEqual(actualData, dataFortesting);

        });

  });



